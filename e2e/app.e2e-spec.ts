import { AwsBitbucketPage } from './app.po';

describe('aws-bitbucket App', () => {
  let page: AwsBitbucketPage;

  beforeEach(() => {
    page = new AwsBitbucketPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
