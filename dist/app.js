var port = process.env.PORT || 3000;

const express = require('express');
const app = express();

app.use(express.static(__dirname));

var server = app.listen(port, function () {
  console.log('Server running at http://127.0.0.1:' + port + '/');
});
