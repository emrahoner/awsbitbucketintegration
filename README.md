# AWS and Bitbucket Integration #

These instructions are for deploying angular 4 application to AWS Elastic Beanstalk services.

Initially, after starting new application and environment in Elastic Beanstalk sevice and creating Bitbucket repository, we will configure the Bitbucket repository.

## Configuring Bitbucket Repository ##

Bitbucket handles the compilation and build process to create deployable package. Therefore, we will configure build process of our angular application here. Initially, you should navigate to Pipelines page of your repository. You will find the link to Pipelines page under main page of your repository. There are configuration options for various programming languages. You need to select javascript for nodejs configuration and after that hit the `Commit` button. Pipeline script named `bitbucket-pipelines.yml` will be committed to your repository automatically. You just need to change `npm test` to `npm build`.

```
image: node:6.9.4

pipelines:
  default:
    - step:
        caches:
          - node
        script:
          - npm install
          - npm build
```

Then, you require deploy script which you can find in [https://bitbucket.org/awslabs/aws-elastic-beanstalk-bitbucket-pipelines-python/src](https://bitbucket.org/awslabs/aws-elastic-beanstalk-bitbucket-pipelines-python/src). You will copy `beanstalk_deploy.py` to root directory of your repository.

In the settings page of the repository, click the `Environment variables` link. In this page, you will add necessary variables for deployment to AWS Beanstalk. These variables are `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, `APPLICATION_NAME`, `APPLICATION_ENVIRONMENT`, `S3_BUCKET`, `AWS_DEFAULT_REGION`.

## Configuring AWS Beanstalk Service ##

To obtain access id and key, you will navigate to `Security Credentials` page which you can find it in popup menu under your profile name at the right top side of the page. Under `Access Keys` panel, click `Create New Access Key` button and a popup page that shows access id and key created will appear. Also you can download id and key as a text file.

In the Elastic Beanstalk main page, you will obtain name of application and environment. For the name of S3 bucket, you should navigate to `S3 Bucket` page, there you will find bucket name. And for the default region name, you will find it in the second part of the url of the application when you split it by dots. Url format is `<application>.<region>.elasticbeanstalk.com`, for example `myapp.us-west-2.elasticbeanstalk.com`. With these six information, you should add environment variables to Bitbucket.

Example variables

| Variable Name           | Value                                    |
| ----------------------- | ---------------------------------------- |
| AWS_ACCESS_KEY_ID       | DE6RTCYWN2VB5SN8USIF                     |
| AWS_SECRET_ACCESS_KEY   | 24nkOfHbRhQjmD0f5m7o4Sp3co4jd9rk4neso04j |
| APPLICATION_NAME        | MyAngularApplication                     |
| APPLICATION_ENVIRONMENT | MyAngularApplication-env                 |
| AWS_DEFAULT_REGION      | us-west-2                                |
| S3_BUCKET               | elasticbeanstalk-us-west-2-882693431731  |

## Edit Pipeline Script for Deployment ##

Pipeline script written previously was for creating deployable package. Now, you should edit it to deploy the package. beanstalk_deploy.py file will be used for deployment, so change it with the script below.

```
image: node:6.9.4

pipelines:
  default:
    - step:
        caches:
          - node
        script:
          - npm install # installs node modules
          - npm install -g @angular/cli # build tools for angular
          - npm run build # builds project and outputs package
          - apt-get update # required to install zip
          - apt-get install -y python-pip # required to install boto3 and run python
          - apt-get install -y zip # required for packaging up the application
          - pip install boto3==1.3.0 # required for beanstalk_deploy.py
          - zip -j /tmp/artifact.zip dist/* # package up the application for deployment
          - python beanstalk_deploy.py # run the deployment script
```